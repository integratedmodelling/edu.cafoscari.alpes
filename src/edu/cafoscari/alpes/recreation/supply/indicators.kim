/* OUTDOOR RECREATION ACTIVITY SUPPLY */
/* Calculation of indicators in k.lab - ARIES project */

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Libraries of concepts imported in the project
private namespace edu.cafoscari.alpes.recreation.supply.indicators
	using im, behavior, conservation, landcover, ecology, earth;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Project knowledge: missing concepts, created for this project
//
//@internal quality HumanInfluence describes earth:Terrain //Hemeroby
//applies to landcover:LandCoverType;
//
//abstract attribute ConservationLevelClassification applies to conservation:ProtectedArea within earth:Region
//has children
// (InternationalClassification 
// 	has children
// 	  StrictConservation, //"Ia"
// 	  WildernessProtection, //"Ib"
// 	  NationalPark, //"II"
// 	  NaturalMonument, //"III"
// 	  HabitatConservation, //"IV"
// 	  LandscapeConservation, //"V"
// 	  ManagedResource) // "VI"
// 	  , //"NA")
// Natura2000Classification;
// 
//@internal quality ProximityToWater is im:Length;
//
//@internal quality Impedence;
//
//@internal quality Ruggedness "Roughness of land surface." is im:Length of earth:Terrain within earth:Terrestrial earth:Region;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*(1) CALCULATE RECREATIONAL VALUE OF PROTECTED AREAS */

// Import the database of National Designated Areas within GAL Alto Bellunese
model vector (file = "data/GAL_CDDA.shp", attr = "IUCNCAT")
	as classify ConservationLevelClassification into 
	  StrictConservation if "Ia",
 	  WildernessProtection if "Ib",
 	  NationalPark if "II",
 	  NaturalMonument if "III",
 	  HabitatConservation if "IV",
 	  LandscapeConservation if "V",
 	  ManagedResource if  "VI";

define RECREATION_PROTECTED_AREA as table (conservation, Recreation_score):
	StrictConservation,	0,	//Strict Nature Reserve: protected area managed mainly for science
	WildernessProtection,	1,	//Wilderness Area: protected area managed mainly for wilderness protection
	NationalPark,		0.8,	//National Park: protected area managed mainly for ecosystem protection and recreation
	NaturalMonument,	1,	//Natural Monument: protected area managed mainly for conservation of specific natural features
	HabitatConservation,	0.8,	//Habitat/Species Management Area: protected area managed mainly for conservation through management intervention
	LandscapeConservation,	0.8,	//Protected Landscape/Seascape: protected area managed mainly for landscape/seascape conservation and recreation
	ManagedResource,	0.8;	//Managed Resource Protected Area: protected area managed mainly for the sustainable use of natural ecosystems

// Model recreation score of protected areas depending on above classification
model value im:Normalized conservation:ProtectedArea 0 to 1
	observing 
	(classify ConservationLevelClassification) named conservation 
	using lookup (conservation) into  RECREATION_PROTECTED_AREA;

// Import raster filter: presence of Natura 2000 areas with recreation score equal to 0.8
model raster (file = "data/GAL_R_Score_N2000.tif", no-data = 0)
	as value Natura2000Classification 0 to 1;

// Generate indicator normalized output 
model value im:Normalized behavior:Recreational conservation:ProtectedArea 0 to 1
	named mnc
	observing
	(value im:Normalized conservation:ProtectedArea 0 to 1) named nc,
	(value Natura2000Classification 0 to 1) named ntc
	on definition set to [
		(nodata(nc))?((nodata(ntc))?(0):(ntc)):(nc)
	];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*(2) RECLASSIFY TO HEMEROBY CLASSES */

// Import Corine land cover map
model vector (file = "data/gal_clc12_4liv_gbo_3035.shp", attr = "CLC_3LVL")
	as classify landcover:LandCoverType into
	landcover:ContinuousUrbanFabric if "111",
	landcover:DiscontinuousUrbanFabric if "112",
	landcover:IndustrialCommercialUnits if "121",
	landcover:RoadRailNetwork if "122",
	landcover:PortArea if "123",
	landcover:Airport if "124",
	landcover:MineralExtraction if "131",
	landcover:DumpArea if "132",
	landcover:ConstructionArea if "133",
	landcover:GreenUrbanArea if "141",
	landcover:SportLeisureFacility if "142",
	landcover:NonIrrigatedArableLand if "211",
	landcover:PermanentlyIrrigatedArableLand if "212",
	landcover:RiceField if "213",
	landcover:Vineyard if "221",
	landcover:FruitAndBerryPlantation if "222",
	landcover:OliveGrove if "223",
	landcover:Pastureland if "231",
	landcover:AnnualCroplandAssociatedWithPermanent if "241",
	landcover:ComplexCultivationPatternedLand if "242",
	landcover:AgriculturalLandWithNaturalVegetation if "243",
	landcover:AgroForestryLand if "244",
	landcover:BroadleafForest if "311",
	landcover:ConiferousForest if "312",
	landcover:MixedForest if "313",
	landcover:NaturalGrassland if "321",
	landcover:MoorAndHeathland if "322",
	landcover:SclerophyllousVegetation if "323",
	landcover:TransitionalWoodlandScrub if "324",
	landcover:BeachDuneAndSand if "331",
	landcover:BareRock if "332",
	landcover:SparseVegetation if "333",
	landcover:BurnedLand if "334",
	landcover:GlacierAndPerpetualSnow if "335",
	landcover:InlandMarsh if "411",
	landcover:PeatBog if "412",
	landcover:SaltMarsh if "421",
	landcover:Saline if "422",
	landcover:IntertidalFlat if "423",
	landcover:Watercourse if "511",
	landcover:WaterBody if "512",
	landcover:CoastalLagoon if "521",
	landcover:Estuary if "522",
	landcover:SeaAndOcean if "523";
	
define HEMEROBY_TABLE as table (landcover, hemeroby):
	landcover:ContinuousUrbanFabric,				7,
	landcover:DiscontinuousUrbanFabric,				7,
	landcover:IndustrialCommercialUnits,				7,
	landcover:RoadRailNetwork,					7,
	landcover:PortArea,						7,
	landcover:Airport,						7,
	landcover:MineralExtraction,					6,
	landcover:DumpArea,						6,
	landcover:ConstructionArea,					6,
	landcover:GreenUrbanArea,					6,
	landcover:SportLeisureFacility,					6,
	landcover:NonIrrigatedArableLand,				5,
	landcover:PermanentlyIrrigatedArableLand,			5,
	landcover:RiceField,						5,
	landcover:Vineyard,						4,
	landcover:FruitAndBerryPlantation,				4,
	landcover:OliveGrove,						4,
	landcover:Pastureland,						4,
	landcover:AnnualCroplandAssociatedWithPermanent,		4,
	landcover:ComplexCultivationPatternedLand,			4,
	landcover:AgriculturalLandWithNaturalVegetation,		4,
	landcover:AgroForestryLand,					4,
	landcover:BroadleafForest,					3,
	landcover:ConiferousForest,					3,
	landcover:MixedForest,						3,
	landcover:NaturalGrassland,					3,
	landcover:MoorAndHeathland,					2,
	landcover:SclerophyllousVegetation,				2,
	landcover:TransitionalWoodlandScrub,				2,
	landcover:BeachDuneAndSand,					2,
	landcover:BareRock,						1,
	landcover:SparseVegetation,					2,
	landcover:BurnedLand,						5,
	landcover:GlacierAndPerpetualSnow,				1,
	landcover:InlandMarsh,						2,
	landcover:PeatBog,						2,
	landcover:SaltMarsh,						2,
	landcover:Saline,						5,
	landcover:IntertidalFlat,					1;
	/*water bodies are not considered - Paracchini and Capitani, 2011 */

// Model human influence depending on above hemeroby classification
model rank HumanInfluence 1 to 7
	observing
		(classify landcover:LandCoverType) named lc
	using lookup (lc) into HEMEROBY_TABLE; 

// Generate indicator normalized output
assess value im:Normalized HumanInfluence 0 to 1
	named nhi
	observing
		(rank HumanInfluence 1 to 7) named hi
	on definition do [
		def max = context.hi.max
		def min = context.hi.min
	 	for (n in context.hi.offsets) {
	 		context.nhi[n\] = (context.hi[n\] - max) / (min - max)
        		} 
	];
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*(3) CALCULATE DISTANCE TO WATER */

// Import main water bodies (lakes) within GAL Alto Bellunese
model each vector (file = "data/GAL_Lakes.shp")
	as earth:WaterBody;

// Calculate euclidean distance to water bodies
model distance to earth:WaterBody in m;

// Set maximum distance to 2000 m since it is assumed as threshold to quantify recreational benefits of being near water bodies
model measure ProximityToWater in m
    observing (distance to earth:WaterBody in m) named dwb
	on definition set to [
	(dwb > 2000)?(2000):(dwb)];

// Model attractiveness of water bodies through an impendence function (Paracchini et al., 2014)
model rank Impedence
	observing (measure ProximityToWater in m) named pw
	on definition set to 
	[(1 + 150) / (150 + Math.exp(3.5*(10**-3)*pw))];	

// Generate indicator normalized output
assess value of im:Normalized ProximityToWater 0 to 1
	named npw
	observing 
	(rank Impedence) named imp
	on definition do [
		def max = context.imp.max
		def min = context.imp.min
	 	for (n in context.imp.offsets) {
	 		context.npw[n\] = (context.imp[n\] - min) / (max - min)
        		}
	];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*(4) CALCULATE NUMBER OF LAND COVER TYPES */

// Import landcover type diversity within GAL Alto Bellunese
model raster (file = "data/GAL_Landcover_Diversity.tif", no-data = -999)
	as rank landcover:LandscapeHeterogeneity;

// Generate indicator normalized output	
assess value im:Normalized landcover:LandscapeHeterogeneity 0 to 1
	named nlh
	observing (rank landcover:LandscapeHeterogeneity) named lh
	on definition do [
		def max = context.lh.max
		def min = context.lh.min
		for (n in context.lh.offsets) {
			context.nlh[n\] = (context.lh[n\] - min) / (max - min)
			}	
		];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*(5) CALCULATE TERRAIN ROUGHNESS */

// Import terrain ruggedness index
model raster (file = "data/GAL_TRI.tif", no-data = -999) // set no-data
	as measure Ruggedness in m;

// Generate indicator normalized output		
assess value im:Normalized Ruggedness 0 to 1
	named nr
	observing (measure Ruggedness in m) named r
	on definition do [
		def max = context.r.max
		def min = context.r.min
		for (n in context.r.offsets) {
			context.nr[n\] = (context.r[n\] - min) / (max - min)
			}	
		];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*(6) CALCULATE DENSITY OF MOUNTAIN PEAKS */

// Import peak count on a square moving window of 10 km per side (
model raster (file = "data/GAL_Peaks_Density_cut.tif", no-data = -9999)
as count earth:MountainPeak per 10*km^2;

// Generate indicator normalized output
assess value im:Normalized earth:MountainPeak 0 to 1
	named nmp
	observing (count earth:MountainPeak per 10*km^2) named mp
	on definition do [
		def max = context.mp.max
		def min = context.mp.min
		for (n in context.mp.offsets) {
			(context.nmp[n\] = (context.mp[n\] - min) / max - min)
		}
	];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*(7) CALCULATE PROXIMITY TO RECREATIONAL AREAS IN TERMS OF TRAVEL TIME */

// Import accessibility to reacreational areas within GAL Alto Bellunese as minutes
model raster (file = 'data/GAL_Accessibility.tif') 
	as measure im:Duration of behavior:TravelConnection in day
	on definition change to [self/(24*60)];

// Generate indicator normalized output
assess value im:Normalized im:Duration of behavior:TravelConnection 0 to 1 named ndt
	observing (measure im:Duration of behavior:TravelConnection in day) named dt
	on definition do [
		def max = context.dt.max
		def min = context.dt.min
		for (n in context.dt.offsets) { 
				(context.ndt[n\] = (context.dt[n\] - max) / (min - max))
			}
		];

